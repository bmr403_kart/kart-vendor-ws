DROP TABLE if exists vendor;
  
CREATE TABLE vendor (
  vendorId BIGINT  AUTO_INCREMENT NOT NULL,
  vendorName VARCHAR(250) NOT NULL,
  vendorDescription VARCHAR(250) NOT NULL,
);