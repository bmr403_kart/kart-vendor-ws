package com.kart.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kart.entity.Vendor;
import com.kart.service.Impl.VendorServiceImpl;

import io.swagger.annotations.ApiOperation;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@RestController
@EnableSwagger2
@RequestMapping(value = "/vendor")
@ComponentScan(basePackages = { "com.kart.controller" })
public class VendorControllerImpl {

	@Autowired
	private VendorServiceImpl vendorServiceImpl;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@GetMapping(value = "/all")
	@ApiOperation(value = "Get All Vendors", nickname = "GET Vendors", 
	notes = "This endpoint retrieves Vendors", produces = "application/json")
	public List<Vendor> getAllVendors(@RequestHeader(value="Accept") String accept, @RequestHeader(value="Accept-Language") String acceptLanguage,
			@RequestHeader(value="User-Agent", defaultValue="foo") String userAgent, HttpServletRequest request ,  HttpServletResponse response ) throws Exception {
		logger.info("I'm in getAllVendors() to return Hello World!");
		List<Vendor> products = vendorServiceImpl.getAllVendors();
		logger.info("I'm in getAllVendors() "+products.size());
		return products;
	}
	
	@PostMapping(value = "/findByIds")
	@ApiOperation(value = "Get All Vendors ById", nickname = "GET Vendors", 
	notes = "This endpoint retrieves Get Vendors ById", produces = "application/json")
	public List<Vendor> getAllProductsById(@RequestBody List<Vendor> vendors, @RequestHeader(value="Accept") String accept, @RequestHeader(value="Accept-Language") String acceptLanguage,
			@RequestHeader(value="User-Agent", defaultValue="foo") String userAgent, HttpServletRequest request ,  HttpServletResponse response ) throws Exception {
		logger.info("I'm in getAllProductsById() to return Hello World!");
		List<Vendor> returnedProducts = vendorServiceImpl.findAllByIds(vendors.stream().map(Vendor::getVendorId).collect(Collectors.toList()));
		logger.info("I'm in getAllProductsById() "+returnedProducts.size());
		return returnedProducts;
	}
	
	@PostMapping(value = "/find")
	@ApiOperation(value = "Get single Vendor", nickname = "GET single Vendor", 
	notes = "This endpoint retrieves single Product", produces = "application/json")
	public Vendor getVendor(@RequestBody Vendor vendor, @RequestHeader(value="Accept") String accept, @RequestHeader(value="Accept-Language") String acceptLanguage,
			@RequestHeader(value="User-Agent", defaultValue="foo") String userAgent, HttpServletRequest request ,  HttpServletResponse response ) throws Exception {
		logger.info("I'm in getVendor() to return Vendor!");
		Vendor resultedProduct = vendorServiceImpl.getOne(vendor.getVendorId());
		logger.info("I'm in getVendor() "+resultedProduct.getVendorName());
		return resultedProduct;
	}
	
	@PostMapping(value = "/findByProductName")
	@ApiOperation(value = "Get single Product", nickname = "GET single Product", 
	notes = "This endpoint retrieves single Product", produces = "application/json")
	public List<Vendor> getVendorByNme(@RequestBody Vendor vendor, @RequestHeader(value="Accept") String accept, @RequestHeader(value="Accept-Language") String acceptLanguage,
			@RequestHeader(value="User-Agent", defaultValue="foo") String userAgent, HttpServletRequest request ,  HttpServletResponse response ) throws Exception {
		logger.info("I'm in getVendorByNme() to return Hello World!");
		List<Vendor> resultedVendors = vendorServiceImpl.findByVendorName(vendor.getVendorName());
		logger.info("I'm in getVendorByNme() "+resultedVendors.size());
		return resultedVendors;
	}
	
	@PostMapping(value = "/save")
	@ApiOperation(value = "Save a Vendor", nickname = "Saves a Vendor", 
	notes = "This endpoint saves a Vendor", produces = "application/json")
	public Vendor saveProduct(@RequestBody Vendor vendor, @RequestHeader(value="Accept") String accept, @RequestHeader(value="Accept-Language") String acceptLanguage,
			@RequestHeader(value="User-Agent", defaultValue="foo") String userAgent, HttpServletRequest request ,  HttpServletResponse response ) throws Exception {
		logger.info("I'm in saveProduct() to return Hello World!");
		Vendor savedVendor = vendorServiceImpl.saveAndFlush(vendor);
		logger.info("I'm in saveProduct() "+savedVendor);
		return savedVendor;
	}
	
	@PostMapping(value = "/saveAll")
	@ApiOperation(value = "Save All Products", nickname = "Saves list of products", 
	notes = "This endpoint saves All Products", produces = "application/json")
	public List<Vendor> saveAllVendors(@RequestBody List<Vendor> vendors, @RequestHeader(value="Accept") String accept, @RequestHeader(value="Accept-Language") String acceptLanguage,
			@RequestHeader(value="User-Agent", defaultValue="foo") String userAgent, HttpServletRequest request ,  HttpServletResponse response ) throws Exception {
		logger.info("I'm in saveAllProducts() to return Hello World!");
		List<Vendor> savedVendors = vendorServiceImpl.saveAll(vendors);
		logger.info("I'm in saveAllProducts() "+savedVendors.size());
		return savedVendors;
	}
	
	@DeleteMapping(value = "/delete")
	@ApiOperation(value = "To Delete Vendors", nickname = "Delete Vendors", 
	notes = "This endpoint deletes Vendor", produces = "application/json")
	public List<Vendor> deleteVendors(@RequestBody List<Vendor> vendors, @RequestHeader(value="Accept") String accept, @RequestHeader(value="Accept-Language") String acceptLanguage,
			@RequestHeader(value="User-Agent", defaultValue="foo") String userAgent, HttpServletRequest request ,  HttpServletResponse response ) throws Exception {
		logger.info("I'm in deleteVendors() to return Hello World!");
		vendorServiceImpl.deleteInBatch(vendors);
		return vendors;
	}
	
	
	
}
